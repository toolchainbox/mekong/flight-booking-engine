import requests, urllib3, datetime, names, uuid, airportsdata, random
from dynaconf import settings, Validator
from collections import Counter
import myLogger

# Variablen definieren
###########################################
airports = airportsdata.load('IATA')
lower_limit = 1
upper_limit = len(airports)




#print(airports['JFK'])
#print(list(airports.values())[0])

# Register validators
###########################################
settings.validators.register(
    # Ensure the database.host field exists.
    #Validator('DATABASE.HOST', must_exist=True),

    Validator("APPCONFIG.URL", default="https://example.com"),
    Validator("APPCONFIG.RUNS", default=10)
)

# Fire the validator
###########################################
settings.validators.validate()

# Warnings abschalten
###########################################
urllib3.disable_warnings()


url = settings.APPCONFIG.URL

# Funktionen
###########################################
def fire_requests(i):
    start = datetime.datetime.now()
    zielFlughafenId = random.randint(lower_limit, upper_limit)
    abflug = airports['FRA']
    ziel = list(airports.values())[zielFlughafenId]
    #print(list(airports.values())[zielFlughafenId])
    #print(abflug)
    #print(ziel)

    data = {
            "loopCounter": i,
            "Vorname": names.get_first_name(),
            "Nachname": names.get_last_name(),
            "Ausweisnummer": str(uuid.uuid4()),
            "Start": abflug["iata"],
            "Ziel": ziel["iata"]
            }

    r = requests.post(url, json=data, verify=False)
    stop = datetime.datetime.now()
    duration = stop - start

    myLogger.info(str("Request #%s / Dauer: %s / Response Code: %s"%(i,duration, r.status_code)))


# Load starten
###########################################
start = datetime.datetime.now()
for i in range(1, settings.APPCONFIG.RUNS+1):
    fire_requests(i)
stop = datetime.datetime.now()

duration = stop - start

myLogger.info(str("CONFIG: %s "%settings.APPCONFIG))
myLogger.info(str("%s Requests in %s verarbeitet. "%(settings.APPCONFIG.RUNS,duration)))