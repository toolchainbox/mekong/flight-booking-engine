PRJ="loadGenerator"

mkdir -p ~/dev/kafka/$PRJ/src

cd  ~/dev/kafka/$PRJ/

python3 -m pip install virtualenv
python3 -m virtualenv ~/dev/kafka/$PRJ/
source ~/dev/kafka/$PRJ/bin/activate


python3 -m pip install -r src/requirements.txt

export DYNACONF_APPCONFIG__RUNS=5
export DYNACONF_APPCONFIG__URL="http://localhost:8080/flights/order"