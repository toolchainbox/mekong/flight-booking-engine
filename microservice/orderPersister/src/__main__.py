import json
import time

from kafka import KafkaConsumer
from dynaconf import settings
import mysql.connector

import myLogger
import initMyDB

while True:
    consumer = KafkaConsumer(settings.APPCONFIG.ORDERTOPIC,
                            bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
                            group_id=settings.APPCONFIG.GROUPID,
                            value_deserializer=lambda m: json.loads(m.decode('utf-8')),
                            auto_offset_reset="earliest",
                            consumer_timeout_ms=60000,
                            enable_auto_commit=False)

    mydb = mysql.connector.connect(
                host=settings.MYSQL.host,
                user=settings.MYSQL.user,
                password=settings.MYSQL.password,
                database="mysql"
            ) 

    for message in consumer:

        mycursor = mydb.cursor()
        sql = "insert into orders.orders (Vorname, Nachname, Ausweisnummer, Start, Ziel) VALUES (%s, %s, %s, %s, %s)"    
        mycursor.execute(sql, message.value)
        mydb.commit()
        mycursor.close()
        myLogger.info(str("DB Eintrag geschrieben: %s"%(str(message.value))))
        consumer.commit()
        myLogger.info(str("Buchung in DB geschrieben: %s"%message.value))

    consumer.close()
    mydb.close()
    time.sleep(10/1000)