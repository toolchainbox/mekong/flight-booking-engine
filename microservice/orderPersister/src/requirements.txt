Flask >= 2.3.2
mysql-connector-python >= 8.0.30
dynaconf >= 3.1.12
json2html>=1.3.0
kafka-python >= 2.0.2
