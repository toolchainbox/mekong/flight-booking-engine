from flask import Flask, request

import myLogger
import myDB
import os

# Variablen definieren
###########################################
app = Flask(__name__)


# Routen
###########################################
@app.route('/')
def hello():
    return myDB.fetchAll()


# App starten
###########################################
if __name__ == '__main__':
    port = os.environ.get('FLASK_PORT') or 8080
    port = int(port)
    app.run(port=port,host='0.0.0.0')