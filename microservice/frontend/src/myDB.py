import mysql.connector
from dynaconf import settings
import myLogger




def fetchAll():
    mydb = mysql.connector.connect(
            host=settings.MYSQL.host,
            user=settings.MYSQL.user,
            password=settings.MYSQL.password,
            database="mysql"
        ) 

    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM orders.orders")
    vals = mycursor.fetchall()
    mycursor.close()
    mydb.close()

    s = ''
    for x in vals:
        s += str(x) + "<br>"
    return s