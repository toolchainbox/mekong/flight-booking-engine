from flask import Flask, request
import os, time, random
import json

from dynaconf import settings
import myLogger
import myDB


from opentelemetry import trace
from opentelemetry.instrumentation.flask import FlaskInstrumentor
from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.export import ConsoleSpanExporter


resource = Resource.create({
    "service.name": "flight-booking-engine",
    "service.instance.id": "instance-1",
})

provider = TracerProvider(resource=resource)
# OTLPSpanExporter(endpoint="http://jaeger:4318", insecure=True)
processor = BatchSpanProcessor(OTLPSpanExporter(endpoint=settings.OTLP.endpoint))
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
tracer = trace.get_tracer("flight-booking.tracer")

instrumentor = FlaskInstrumentor()


# Variablen definieren
###########################################
app = Flask(__name__)


# auto-instrumentation tracing
# calling this method directly without passing any optional values does the very same thing that the opentelemetry-instrument command does
instrumentor.instrument_app(app)


# Funktionen
###########################################
def fraud_check():
    with tracer.start_as_current_span("fraud_check"):
        lower_limit = 0
        upper_limit = 1000
        number = random.randint(lower_limit, upper_limit)
        time.sleep(number  / 1000)

def amadeus_bookFlight():
    with tracer.start_as_current_span("amadeus_bookFlight"):
        lower_limit = 7
        upper_limit = 12
        number = random.randint(lower_limit, upper_limit)
        time.sleep(number)

def flight_booking():
    with tracer.start_as_current_span("flight_booking"):
        lower_limit = 1
        upper_limit = 1000
        number = random.randint(lower_limit, upper_limit)
        time.sleep(number  / 1000)
        
        mydata = request.get_json()

        val = (mydata["Vorname"], 
                mydata["Nachname"],
                mydata["Ausweisnummer"],
                mydata["Start"],
                mydata["Ziel"])
        myDB.insertOrder(val)



# Routen
###########################################
@tracer.start_as_current_span("/")
@app.route('/')
def hello():
    return myDB.fetchAll()

@tracer.start_as_current_span("/flights/order")
@app.route('/flights/order', methods=['POST'])
def flightsbooking():
    #print(request.data)
    fraud_check()
    amadeus_bookFlight()
    flight_booking()
    return str("Done %s "%(request.data))


# App starten
###########################################
if __name__ == '__main__':
    port = os.environ.get('FLASK_PORT') or 8080
    port = int(port)
    app.run(port=port,host='0.0.0.0')